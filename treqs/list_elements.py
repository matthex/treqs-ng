import os
import xml.etree.cElementTree as ET
import logging

from file_traverser import file_traverser

class list_elements:

    def __init__(self):
        self.logger = logging.getLogger('chalmers-treqs.treqs-ng')
        self.traverser = file_traverser()
        self.logger.info('list_elements created')

    def list_elements(self, file_name, recursive):
         self.traverser.traverse_file_hierarchy(file_name, recursive, self.log_element_info, self.traverser.traverse_XML_file)
        
    def log_element_info(self, file_name, element):
        self.logger.info('| %s | %s | %s |', element.get("type"), self.extract_label(element.text), element.get("id"))

    def extract_label(self, text):
        # It is our convention to use the first none-empty line as label
        ret = "None"
        for line in text.splitlines():
            if line != "" and ret == "None":
                ret = line

        return ret

if __name__ == "__main__":
   le = list_elements()
   le.list_elements("../requirements/treqs-system-requirements.md", "true")
