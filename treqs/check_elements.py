import os
import xml.etree.cElementTree as ET
import json
import logging
import sys

from file_traverser import file_traverser

class check_elements:

    def __init__(self):
        self.logger = logging.getLogger('chalmers-treqs.treqs-ng')
        self.traverser = file_traverser()
        self.ttim_types=dict()
        self.ttim_required=dict()
        self.type_to_element_mapping=dict()
        # self.tested_reqs=dict()
        self.logger.info('check_elements created')

    def check_elements(self, file_name, recursive, ttim_path):
        self.logger.info('Processing TTIM at %s', ttim_path)
        success = self.load_ttim(ttim_path)

        if success == 0:
            success = self.traverser.traverse_file_hierarchy(file_name, recursive, self.check_treqs_element, self.traverser.traverse_XML_file)

        if success == 0:
            self.logger.info("treqs check exited successfully.")
        else: 
            self.logger.warning("treqs check exited with failed checks.")
            
        sys.exit(success)
        #TODO: We need to discuss how to enable post-traversal checks
        # # Do post-traversal check here
        # for current_type, current_elements in self.type_to_element_mapping.items():
        #     #Hardcoded type to test
        #     if current_type == "requirement":
        #         for current_req in current_elements:
        #             if (not current_req in self.tested_reqs):
        #                 print("The following requirement has no associated tests: %s" % current_req)
        # print(self.type_to_element_mapping)
        # print(self.tested_reqs)

    def check_treqs_element(self, file_name, element):
        success = 0
        #For all treqs elements, collect all their links
        #TODO: ASSUMPTION: we only consider links as direct childs of a treqs-element
        if element.tag == "treqs-element":
            #Build a dict of type names to ids (keep track of which elements exist for which types)
            #To be used for post-traversal checks - currently useless
            if not element.get("type") in self.type_to_element_mapping:
                self.type_to_element_mapping[element.get("type")] = [element.get("id")]
            else:
                self.type_to_element_mapping[element.get("type")].append(element.get("id"))

            #Only process types that are recognized per TTIM
            if not element.get("type") in self.ttim_types:
                self.logger.warning('Unrecognized type: %s (File %s)', element.get("type"), file_name)
                return 1
            else:
                #print("Type supported: %s" % (element.get("type")))
                missing_traces = self.ttim_required[element.get("type")]
                # print("This tag requires the following links: %s" % self.ttim_required[element.get("type")])

                for link in element.iter('treqs-link'):
                    #print("LINK: | %s |" % (link.get("target"))) 

                    #Only process link types that are recognized per TTIM
                    if not link.get("type") in self.ttim_types[element.get("type")]:
                        self.logger.warning('Unrecognized link type %s within an element of type %s (File %s)', link.get("type"), element.get("type"), file_name) 
                        success = 1
                    else:
                        #"Cross off" required links
                        #Check whether outgoing required traces exist
                        if link.get("type") in missing_traces:
                            missing_traces.remove(link.get("type"))

                        # #TODO: HARDCODED! For now, I just hardcoded a single type and relationship to test. Read later as a dynamic config TIM/RIM file.
                        # if (element.get("type") == "unittest" and link.get("type") == "tests"):
                        #     print("Unit test tests a requirement: %s to %s" % (element.get("id"), link.get("target")))
                        #     self.tested_reqs[link.get("target")] = element.get("id")

                        #TODO: For each type
                        #TODO: Extract all ids of that type
                        #TODO: For each element of that type, save traces that are potentially required as incoming --> Save

                        #TODO: For all incoming required traces, process the id trace collections

                if len(missing_traces) > 0:
                    self.logger.warning('Links missing for %s: %s (File %s)', element.get("id"), missing_traces, file_name)
                    success = 1
              
        return success

    def load_ttim(self, ttim_path):
        if os.path.isfile(ttim_path):
            # Open the ttim
            with open(ttim_path) as ttim_file:
                ttim_json = json.load(ttim_file)

                #Process TTIM    
                for current_type in ttim_json["types"]:
                    #Extract all types
                    self.ttim_types[current_type['name']] = []
                    self.ttim_required[current_type['name']] = []

                    #TODO: Add support for required traces (A "type" needs to "relateTo" another "type")
                    #TODO: Add support for incoming required traces (A "type" needs to have a "relateTo" from at least one other "type")

                    #For each type, save all allowed trace types (outgoing traces)
                    for current_link in current_type["links"]:
                        self.ttim_types[current_type['name']].append(current_link["type"])
                        if "required" in current_link.keys():
                            self.ttim_required[current_type['name']].append(current_link["type"])

                self.logger.info('Types and their links according to TTIM: %s', self.ttim_types)
                return 0

        #TODO: we want to devise a strategy for graceful error handling at some point
        else:
            self.logger.error('TTIM could not be loaded at %s.', ttim_path)
            return 1

    def extract_label(self, text):
        # It is our convention to use the first none-empty line as label
        ret = "None"
        for line in text.splitlines():
            if line != "" and ret == "None":
                ret = line

        return ret

if __name__ == "__main__":
   ce = check_elements()
   ce.check_elements("../requirements/treqs-system-requirements.md", "true", "./ttim.json")
