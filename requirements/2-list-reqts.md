<treqs>
<treqs-element id="a0820e06-9614-11ea-bb37-0242ac130002" type="requirement" >

### 2.0 Parameters and default output of treqs list

    Usage: treqs list [OPTIONS]
    
      List treqs elements in this folder
    
    Options:
      --type TEXT       Limit action to specified treqs element type
      --recursive TEXT  List treqs elements recursively in all subfolders.
      --filename TEXT   Give a file or directory to list from.
      --help            Show this message and exit.

If type is omitted, all treqs elements, regardless of type, will be returned.

The default value for recursive is 'true'. Unless otherwise specified, treqs shall list elements recursively.

Filename can either provide a directory or a file. If filename is omitted, treqs defaults to '.', i.e. the current working directory.
<treqs-link type="hasParent" target="35590bca-960f-11ea-bb37-0242ac130002" />

</treqs-element>
<treqs-element id="63ef8bfa76ae11ebb811cf2f044815f7" type="requirement">

### 2.1 Information listed by treqs list

When listing treqs-elements, treqs shall list the following information as a markdown table:

  - element type, 
  - the label (i.e. the first non-empty line of a treqs element), and 
  - the id of the element (the UID provided by the treqs create command). 

> Example:

| Element type | Label | UID |
| :--- | :--- | :--- |
| requirement | ### 2.0 Parameters and default output of treqs list | a0820e06-9614-11ea-bb37-0242ac130002 |
| requirement | ### 2.1 Filter by type  | 437f09c6-9613-11ea-bb37-0242ac130002 |
| requirement | ### 2.2 Filter by ID  | a0820b4a-9614-11ea-bb37-0242ac130002 |
| requirement | ### 2.3 List all elements in a file | bc89e02a76c811ebb811cf2f044815f7 |
| requirement | ### 2.4 List treqs elements in a directory | 638fa22e76c911ebb811cf2f044815f7 |

<treqs-link type="hasParent" target="35590bca-960f-11ea-bb37-0242ac130002" />

</treqs-element>

<treqs-element id="437f09c6-9613-11ea-bb37-0242ac130002" type="requirement" >

### 2.2 Filter by type (todo)
TReqs shall allow to list treqs-elements within a specified gitlab project that have a specific type (e.g. requirement, test, quality requirement).
<treqs-link type="hasParent" target="35590bca-960f-11ea-bb37-0242ac130002" />

> Example:

    treqs list --type=requirement

</treqs-element>

<treqs-element id="a0820b4a-9614-11ea-bb37-0242ac130002" type="requirement" >

### 2.3 Filter by ID (todo)
TReqs shall allow to list treqs-elements within a specified gitlab project that have a specific ID.
<treqs-link type="hasParent" target="35590bca-960f-11ea-bb37-0242ac130002" />

> Example:

    treqs list --id="35590bca-960f-11ea-bb37-0242ac130002"

</treqs-element>

<treqs-element id="bc89e02a76c811ebb811cf2f044815f7" type="requirement">

### 2.4 List all elements in a file

If a file is given, treqs will list all treqs elements in that file.
<treqs-link type="hasParent" target="35590bca-960f-11ea-bb37-0242ac130002" />
<treqs-link type="relatesTo" target="a0820e06-9614-11ea-bb37-0242ac130002" />
</treqs-element>

<treqs-element id="638fa22e76c911ebb811cf2f044815f7" type="requirement">


### 2.5 List treqs elements in a directory

If a directory is given, treqs will list all treqs elements in all files of this directory. 
If the recursive parameter is given, treqs will list also all treqs elements in all subdirectories of the given directory.
<treqs-link type="hasParent" target="35590bca-960f-11ea-bb37-0242ac130002" />
<treqs-link type="relatesTo" target="bc89e02a76c811ebb811cf2f044815f7" />
</treqs-element>
</treqs>
