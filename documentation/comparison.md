| Feature | treqs | doorstop | word processor | spreadsheet | traditional re tools | 
| ---     | ---   | ---      | ---            | ---         | ---                  |
| **Traceability** |
| Supports unique IDs | yes (UID) | yes (user defined pattern, use filename and git to avoid ID collisions) | no | no | yes |
| Supports traceability | yes | yes | no | to some extend, through handwritten ids  | yes |
| Allows complex tracelinks, e.g. additional attributes and types | yes | no | no | no | yes |
| **Version control** |
| Supports baselining | yes, through git | yes, through git | to some extend, through sharepoint and change control | no | yes |
| Integrates with code and tests on git | yes | partially (*) | no | no | no |
| **Interoperability and recoverability** | 
| Needs/supports additional scripting and infrastructure to integrate in processes | yes | yes | no | partially | yes |
| Scripting interface | forthcoming, python | yes, python | no | partially | yes, proprietory |
| Requirements stored in an accessible format | yes | yes (*) | yes | yes | no |
| Requirements stored in a human readable format | yes | no (*) | yes | yes | no |
| Open source | yes, MIT | yes, LGPL | no | no | no |
| **Collaboration at scale** |
| Can easily provide rw access to a large number of developers | yes | yes | no | no | no |
| Generate views and reports | forthcoming | yes, html | no | no | yes |
| Allows peer-reviewing of requirements between Developers | yes | yes (**) | no | no | no | 
| **Advanced features** | 
| Consistency checks | yes | yes | no | no | yes |
| Allows adjusting of metamodel | yes, per project | no | no | no | yes, per instance |
| Allows graphical modeling | yes, plantuml | unsure | yes | partially | yes |
| Allows to review changes and merging of graphical modeling | yes, plantuml diff in git merge | unsure | no | no | no |
| Support for regulation and standards | commercial options forthcoming (considering ISO26262, SOTIF, HiPAA, ISO 27001) | unsure | no | no | usually yes | 

(*)
: Requirements are managed in yaml configuration files, that do not emphasize human readability in the same way as treqs' markdown files. To see requirements in their context, one needs to generate the html document

(**)
: Doorstop has a more formal view on reviews of requirements and tracelinks, taking note of which item has been reviewed and confirmed. It has a strict, yet static state model about requirements as well. In contrast, treqs treats requirements similar to code in git - where you would also not review and confirm individual lines (unless you are in a cleanroom scenario). These are different philosophies, treqs perhaps pushing more for an agile mindset and doorstop more for a strict, plan-driven mindset.
